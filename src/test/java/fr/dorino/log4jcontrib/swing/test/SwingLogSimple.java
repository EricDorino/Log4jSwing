
package fr.dorino.log4jcontrib.swing.test;

import fr.dorino.log4jcontrib.swing.ui.LogPanel;
import java.awt.BorderLayout;
import org.apache.logging.log4j.*;

public class SwingLogSimple extends javax.swing.JFrame {

    private static final Logger LOGGER = LogManager.getLogger();
    
    public SwingLogSimple() {
        initComponents();
        getContentPane().add(BorderLayout.CENTER, LogPanel.getInstance());       
    }

    public void run() {
        for (int i = 0; i < 100; i++)
            if (i % 5 == 0)
                LOGGER.error(String.format("Error #%d", i));
            else if (i % 3 == 0)
                LOGGER.warn(String.format("Warn #%d", i));
            else
                LOGGER.info(String.format("Info #%d", i));
    }

    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        setDefaultCloseOperation(javax.swing.WindowConstants.EXIT_ON_CLOSE);

        pack();
    }// </editor-fold>//GEN-END:initComponents

    public static void main(String args[]) {
        /* Set the Nimbus look and feel */
        //<editor-fold defaultstate="collapsed" desc=" Look and feel setting code (optional) ">
        /* If Nimbus (introduced in Java SE 6) is not available, stay with the default look and feel.
         * For details see http://download.oracle.com/javase/tutorial/uiswing/lookandfeel/plaf.html 
         */
        try {
            for (javax.swing.UIManager.LookAndFeelInfo info : javax.swing.UIManager.getInstalledLookAndFeels()) {
                if ("Nimbus".equals(info.getName())) {
                    javax.swing.UIManager.setLookAndFeel(info.getClassName());
                    break;
                }
            }
        } catch (ClassNotFoundException ex) {
            java.util.logging.Logger.getLogger(SwingLogSimple.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (InstantiationException ex) {
            java.util.logging.Logger.getLogger(SwingLogSimple.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (IllegalAccessException ex) {
            java.util.logging.Logger.getLogger(SwingLogSimple.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (javax.swing.UnsupportedLookAndFeelException ex) {
            java.util.logging.Logger.getLogger(SwingLogSimple.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        }
        //</editor-fold>
        //</editor-fold>

        /* Create and display the form */
        java.awt.EventQueue.invokeLater(new Runnable() {
            @Override
            public void run() {
                SwingLogSimple frame = new SwingLogSimple();
                frame.setSize(800,600);
                frame.setVisible(true);
                frame.run();                
            }
        });
    }

    // Variables declaration - do not modify//GEN-BEGIN:variables
    // End of variables declaration//GEN-END:variables
}
