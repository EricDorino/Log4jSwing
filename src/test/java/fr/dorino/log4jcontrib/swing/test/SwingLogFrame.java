
package fr.dorino.log4jcontrib.swing.test;

import org.apache.logging.log4j.*;

public class SwingLogFrame {
    private static final Logger LOGGER = LogManager.getLogger();

    public static void main(String[] args) { 
        LOGGER.info("Starting...");
        LOGGER.info("Information");
        LOGGER.info("Stopping...");
    }    
}
