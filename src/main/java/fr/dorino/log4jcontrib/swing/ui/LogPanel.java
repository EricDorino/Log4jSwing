
package fr.dorino.log4jcontrib.swing.ui;

import java.awt.Color;
import org.apache.logging.log4j.*;
import java.awt.Rectangle;
import java.util.HashMap;
import javax.swing.*;
import javax.swing.text.*;

public class LogPanel extends JScrollPane implements LogWidget {
    private static final Logger LOGGER = LogManager.getLogger();    
    
    private static LogPanel instance;

    public static LogPanel getInstance() {    
        if (instance == null) {
            synchronized(LogPanel.class) {
                if(instance == null) {
                    instance = new LogPanel();
        	}
            }
        }
        return instance;
    }
    
    private final HashMap<String, Style> styles;
    
    private LogPanel() {
        super(new JTextPane(), 
            ScrollPaneConstants.VERTICAL_SCROLLBAR_ALWAYS, 
            ScrollPaneConstants.HORIZONTAL_SCROLLBAR_AS_NEEDED);
        
        JTextPane textPane = (JTextPane)getViewport().getComponent(0);
        
        styles = new HashMap<>();
        
        Style style;
        styles.put(WARN, style = textPane.addStyle(WARN, null));
        StyleConstants.setForeground(style, Color.RED);
        
        styles.put(LogWidget.ERROR, style = textPane.addStyle(LogWidget.ERROR, null));
        StyleConstants.setForeground(style, Color.RED);
        StyleConstants.setBold(style, true);
    }

    @Override
    public void logger(String level, String message) {
        try {
            JTextPane textPane = (JTextPane)getViewport().getComponent(0);            
            
            Document doc = textPane.getStyledDocument();
            Position end = doc.getEndPosition();
            doc.insertString(end.getOffset()-1, message, styles.get(level));           
            textPane.scrollRectToVisible(
                    new Rectangle(0, textPane.getHeight(), 1, 1));            
        } catch(BadLocationException e) {
            LOGGER.error("Bad Location Exception : " + e.getMessage());
        }
    }			
	
    @Override
    public void setVisible(boolean visible) {
        // Always visible
    }
}