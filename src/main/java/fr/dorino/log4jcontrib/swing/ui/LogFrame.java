
package fr.dorino.log4jcontrib.swing.ui;

import org.apache.logging.log4j.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.BorderLayout;
import java.awt.Color;
import java.util.*;
import javax.swing.*;
import javax.swing.text.*;

public class LogFrame implements LogWidget {
    private static final Logger LOGGER = LogManager.getLogger();
    private final JFrame jframe;
    private JButton startPause;
    private JButton stop;
    private JButton clear;
    private JButton search;
    private JTextField searchField;
    private JPanel buttonsPanel;
    private JTextPane logMessages;
    private JScrollPane scrollPane;
    private final java.util.List logBuffer; 

    public static final int STARTED = 0;
    public static final int PAUSED = 1;
    public static final int STOPPED = 2;    
    private int state;

    public static final String STYLE_REGULAR = "regular";
    public static final String STYLE_HIGHLIGHTED = "highlighted";
    public static final String START = "start";
    public static final String PAUSE = "pause";
    public static final String STOP = "stop";
    public static final String CLEAR = "clear";
    public static final String SEARCH = "search";


    private static LogFrame instance;

    public static LogFrame getInstance() {    
        if (instance == null) {
            synchronized(LogFrame.class) {
                if(instance == null) {
                    instance = new LogFrame();
        	}
            }
        }
        return instance;
    }
    
    private LogFrame() {
	logBuffer = new ArrayList();
	state = STARTED;
		
	jframe = new JFrame();
	jframe.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);		
	initButtonsPanel();	
	initMessagesArea();		
	jframe.getContentPane().add(BorderLayout.NORTH, buttonsPanel);
	jframe.getContentPane().add(BorderLayout.CENTER, scrollPane);
	jframe.setSize(800,600);
	jframe.setVisible(true);
    }

    @Override
    public void logger(String level, String message) {
    	if(state == STARTED) {
            try {
                StyledDocument sDoc = logMessages.getStyledDocument();
		if (!logBuffer.isEmpty()) {
                    Iterator iter = logBuffer.iterator();
                    while(iter.hasNext()) {					
                        sDoc.insertString(0, (String)iter.next(), sDoc.getStyle(STYLE_REGULAR));			
                        iter.remove();
                    }
		}			
		sDoc.insertString(0, message, sDoc.getStyle(STYLE_REGULAR));
            } catch(BadLocationException e) {
                LOGGER.error("Bad Location Exception : " + e.getMessage());
            }
	}
	else if(state == PAUSED) {
            logBuffer.add(message);
	}
    }
	
    private void initButtonsPanel() {
        buttonsPanel = new JPanel();
	startPause = new JButton(PAUSE);
	startPause.addActionListener(new StartPauseActionListener());
	stop = new JButton(STOP);
	stop.addActionListener((ActionEvent evt) -> {
            state = STOPPED;
            startPause.setText(START);
        });
        clear = new JButton(CLEAR);
        clear.addActionListener((ActionEvent evt) -> {
            logMessages.setText("");
        });        
        searchField = new JTextField(25);
        search = new JButton(SEARCH);
        search.addActionListener(new SearchActionListener());
	buttonsPanel.add(startPause);
	buttonsPanel.add(stop);
        buttonsPanel.add(clear);
        buttonsPanel.add(searchField);
        buttonsPanel.add(search);    
    }

    private void initMessagesArea() {
	logMessages = new JTextPane();
	scrollPane = new JScrollPane(logMessages);
	scrollPane.setVerticalScrollBarPolicy(ScrollPaneConstants.VERTICAL_SCROLLBAR_ALWAYS);
	scrollPane.setHorizontalScrollBarPolicy(ScrollPaneConstants.HORIZONTAL_SCROLLBAR_NEVER);
	StyledDocument sDoc = logMessages.getStyledDocument();
	Style def = StyleContext.getDefaultStyleContext().getStyle(StyleContext.DEFAULT_STYLE);		
	Style s1 = sDoc.addStyle(STYLE_REGULAR, def);
	StyleConstants.setFontFamily(def, "SansSerif");
	
	Style s2 = sDoc.addStyle(STYLE_HIGHLIGHTED, s1);
	StyleConstants.setBackground(s2, Color.BLUE);		
    }
	
    class StartPauseActionListener implements ActionListener {
        @Override
        public void actionPerformed(ActionEvent evt) {
            JButton srcButton = (JButton)evt.getSource();
            if (START.equals(srcButton.getText())) {
                srcButton.setText(PAUSE);
		state = STARTED;
            }
            else { // PAUSE.equals(srcButton.getText())
                state = PAUSED;
		srcButton.setText(START);
            }
	}
    }
	
    class SearchActionListener implements ActionListener {
        @Override
	public void actionPerformed(ActionEvent evt) {
            JButton srcButton = (JButton)evt.getSource();
            if (!SEARCH.equals(srcButton.getText())) {
                return;
            }
            String searchTerm = searchField.getText();
            String allLogText = logMessages.getText();
            int startIndex = 0, selectionIndex;
            Highlighter hLighter = logMessages.getHighlighter();
            hLighter.removeAllHighlights();
            DefaultHighlighter.DefaultHighlightPainter highlightPainter = 
                    new DefaultHighlighter.DefaultHighlightPainter(Color.BLUE);
            while ((selectionIndex = allLogText.indexOf(searchTerm, startIndex)) != -1) {
                startIndex = selectionIndex + searchTerm.length();
		try {
                    int newLines = getNumberOfNewLinesTillSelectionIndex(allLogText, selectionIndex);
                    hLighter.addHighlight(selectionIndex-newLines, (selectionIndex+searchTerm.length()-newLines), highlightPainter);
		} catch(BadLocationException e) {
                    LOGGER.error("Bad Location Exception: " + e.getMessage());							
		}
            }
	}

	private int getNumberOfNewLinesTillSelectionIndex(String allLogText, int selectionIndex) {
            int numberOfNewlines = 0;
            int pos = 0;
            while((pos = allLogText.indexOf("\n", pos))!=-1 && pos <= selectionIndex) {
                numberOfNewlines++;
		pos++;
            }
            return numberOfNewlines;
        }
		
    }
	
    @Override
    public void setVisible(boolean visible) {
        jframe.setVisible(visible);
    }
}