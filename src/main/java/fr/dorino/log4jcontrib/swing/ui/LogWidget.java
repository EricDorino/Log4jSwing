
package fr.dorino.log4jcontrib.swing.ui;

public interface LogWidget {
    public static final String INFO = "Info";
    public static final String WARN = "Warn";
    public static final String ERROR = "Error";
    //public static LogWidget getInstance()
    public void logger(String level, String message);
    public void setVisible(boolean visible);
}
