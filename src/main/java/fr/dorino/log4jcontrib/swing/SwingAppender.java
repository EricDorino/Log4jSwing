
package fr.dorino.log4jcontrib.swing;

import fr.dorino.log4jcontrib.swing.ui.*;
import org.apache.logging.log4j.core.*;
import org.apache.logging.log4j.core.appender.AbstractAppender;
import org.apache.logging.log4j.core.layout.PatternLayout;
import org.apache.logging.log4j.core.config.plugins.*;
import java.io.Serializable;
import org.apache.logging.log4j.Level;

@Plugin(name = "Swing", category = "Core", elementType = "appender", printObject = true)
public class SwingAppender extends AbstractAppender {

    private final LogWidget logWidget;

    private SwingAppender(String name,
                         Filter filter,
                         Layout<? extends Serializable> layout,
                         boolean ignoreExceptions,
                         String type) {
        super(name, filter, layout, ignoreExceptions);
        if (type.equalsIgnoreCase("Panel"))
            logWidget = LogPanel.getInstance();        
        else
            logWidget = LogFrame.getInstance();
    }
 
    @PluginFactory
    public static SwingAppender createAppender(
            @PluginAttribute("name") String name,
            @PluginAttribute("ignoreExceptions") boolean ignoreExceptions,
            @PluginElement("Layout") Layout layout,
            @PluginElement("Filters") Filter filter,
            @PluginAttribute("type") String type) {
        if (name == null) {
            LOGGER.error("No name provided for SwingAppender");
            return null;
        }
        if (layout == null) {
            layout = PatternLayout.createDefaultLayout();
        }
        if (type == null) {
            type = "Frame";
        }
        return new SwingAppender(name, filter, layout, ignoreExceptions, type);
    }    
    
    private String level(LogEvent event) {
        Level l = event.getLevel();
        if (l == Level.WARN) {
            return LogWidget.WARN;
        }
        else if (l == Level.FATAL || l == Level.ERROR) {
            return LogWidget.ERROR;
        }        
        else 
            return LogWidget.INFO;        
    }
    
    @Override
    public void append(LogEvent event) {        
        logWidget.logger(level(event), new String(getLayout().toByteArray(event)));
    }
            
}